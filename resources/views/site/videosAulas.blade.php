@extends('layouts.site')

@section('titulo','videosAulas')

@section('content')

<h2 id="vd">Videos Aulas</h2>
<div class="container">
<div class="row">
    <div class="col-5">
        <iframe width="250" height="150" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="col-7">
        <h3>Curso de HTML e CSS para iniciantes Aula</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis laudantium sint nulla delectus officiis, placeat inventore fugiat quia quasi nihil sapiente doloribus expedita molestias, adipisci vero aperiam nisi qui ab?</p>
        
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4 col-md-4 mt-3">
        <iframe width="250" height="150" src="https://www.youtube.com/embed/ze9--J4PJ_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>GitHub, GitLab ou Bitbucket?
            Qual nós usamos! // CAC #6
        </h4>
  </div>
  
  <div class="col-lg-4 col-md-4 mt-3">
    <iframe width="250" height="150" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <h4>MVC // Dicionário do
        Programador
    </h4>
  </div>
  
  <div class="col-lg-4 col-md-4 mt-3 ">
    <iframe width="250" height="150" src="https://www.youtube.com/embed/5K7OGSsWlzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <h4>O que um Analista de
        Sistema faz? // Vlog #77
        </h4>
    </div>
  
 </div>
</div>







@endsection