<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
})->name('home');

Route::get('/html', function () {
    return view('site.html');
})->name('html');

Route::get('/javascripts', function () {
    return view('site.javascripts');
})->name('javascripts');

Route::get('/dica-css', function () {
    return view('site.css');
})->name('dica-css');

Route::get('/videosAulas', function () {
    return view('site.videosAulas');
})->name('videosAulas');

Route::get('/contato', function () {
    return view('site.contato');
})->name('contato');
